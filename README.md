# USB C Bench Power Supply

A USB Type-C powered Bench Power Supply.

Hook it up to a USB C-PD adapter that supports 20V and there you go! Goes nice with my USB-C Powerbrick project too!