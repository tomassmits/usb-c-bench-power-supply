boxWidth = 90;
boxHeight = 70;
boxDepth = 55;

terminalHoleHeight = 13;
terminalHoleDiameter = 6.5;
terminalHoleDistance = 19.05; // 1/4 inch - General Radio "double banana" distance

dpsBottomHeight = 26;
dpsWidth = 72;
dpsHeight = 39.55;
dpsCutWidth = 76-dpsWidth;
dpsCutHeight = 13;
dspCutBottomWidth = 25;

dspPlateauDepth = 20;
dspPlateauWidth = 20;

//wedgeDepth = boxDepth-37;   // TODO: Change this to 0 to make the box squared (easier to print).
wedgeDepth = 0;

wedgeHeight = wedgeDepth;

wallThickness = 2.5;
frontWallThickness = wallThickness+0.5;

ventSlotWidth = 2;
ventSlotDepth = 20;
ventSlotDepthStart = 23;
ventSlotHeightStart = boxHeight-wedgeHeight-wallThickness;
ventSlotHeightStart2 = 15;
ventSlotHeightEnd2 = boxHeight/2-ventSlotHeightStart2;
ventSlotCount = 12;

mountingHoleDiameter = 3;
mountingHoleOuterDiameter = 5;