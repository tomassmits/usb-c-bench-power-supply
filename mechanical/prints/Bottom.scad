use <handyCubes.scad>;
include <config.scad>;
//$fn=100;

module bottomBox() {
    margin = 1;
    difference() {
        
        // Outer box
        difference() {
            translate( [0,boxDepth/2.0,0] )
                cCube( [boxWidth,boxDepth,boxHeight] );

            translate( [boxWidth/2.0+margin,boxDepth-wedgeDepth,boxHeight] )
                rotate(v=[0,0,1], a=90)
                    hCube( [wedgeDepth,boxWidth+margin*2,-wedgeHeight] );
        }

        // Big inner cutout
        translate( [0,boxDepth/2+frontWallThickness,wallThickness] )
            cCube( [boxWidth-wallThickness*2,boxDepth,boxHeight-wallThickness*2] );
        
        // DPS big slot
        translate( [0,margin,dpsBottomHeight] )
            cCube( [dpsWidth,wallThickness+margin*2,dpsHeight] );
        
        // DPS slot cutout
        translate( [dpsWidth/2,margin,dpsBottomHeight+dpsHeight/2-dpsCutHeight/2] )
            cCube( [dpsCutWidth,wallThickness+margin*2,dpsCutHeight] );
        translate( [-dpsWidth/2,margin,dpsBottomHeight+dpsHeight/2-dpsCutHeight/2] )
            cCube( [dpsCutWidth,wallThickness+margin*2,dpsCutHeight] );
        // Bottom
        translate( [0,margin,dpsBottomHeight-0.5] )
            cCube( [dspCutBottomWidth,wallThickness+margin*2,1] );
        
        // Terminal holes
        translate( [-terminalHoleDistance/2,-margin,terminalHoleHeight] )
            rotate( v=[1,0,0],a=270 )
            cylinder( h=wallThickness+margin*2, d=terminalHoleDiameter );
        translate( [terminalHoleDistance/2,-margin,terminalHoleHeight] )
            rotate( v=[1,0,0],a=270 )
            cylinder( h=wallThickness+margin*2, d=terminalHoleDiameter );
            
        // Vent slots
        ventDepth = ventSlotDepth;
        for( i = [0:ventSlotCount-1]) {
            // Vent slots top
            x = -(ventSlotCount-1)/2*ventSlotWidth*2 + i*ventSlotWidth*2;
            translate( [x,ventDepth/2+ventSlotDepthStart-wallThickness,ventSlotHeightStart] )
                cCube( [ventSlotWidth,ventDepth, boxHeight] );
        }

    }
    
    // DSP plateau
    // Bottom
    translate( [boxWidth/2-dspPlateauWidth/2,margin+dspPlateauDepth/2+frontWallThickness,dpsBottomHeight-wallThickness] )
        cCube( [dspPlateauWidth,dspPlateauDepth+margin*2,wallThickness] );
    translate( [-boxWidth/2+dspPlateauWidth/2,margin+dspPlateauDepth/2+frontWallThickness,dpsBottomHeight-wallThickness] )
        cCube( [dspPlateauWidth,dspPlateauDepth+margin*2,wallThickness] );
    // Top
    translate( [boxWidth/2-dspPlateauWidth/2,margin+dspPlateauDepth/2+frontWallThickness,dpsBottomHeight+dpsHeight] )
        cCube( [dspPlateauWidth,dspPlateauDepth+margin*2,wallThickness] );
    translate( [-boxWidth/2+dspPlateauWidth/2,margin+dspPlateauDepth/2+frontWallThickness,dpsBottomHeight+dpsHeight] )
        cCube( [dspPlateauWidth,dspPlateauDepth+margin*2,wallThickness] );
    
    
    // Backplane mounting holes
    translate( [boxWidth/2-wallThickness-mountingHoleOuterDiameter/2, 0, wallThickness] )
        mountingHole();
    translate( [-boxWidth/2+wallThickness+mountingHoleOuterDiameter/2, 0, wallThickness] )
        mountingHole();
    translate( [boxWidth/2-wallThickness-mountingHoleOuterDiameter/2, 0, boxHeight-wallThickness*2-mountingHoleOuterDiameter/2] )
        mountingHole();
    translate( [-boxWidth/2+wallThickness+mountingHoleOuterDiameter/2, 0, boxHeight-wallThickness*2-mountingHoleOuterDiameter/2] )
        mountingHole();
        
    // USB C PCB holder
    translate( [0,boxDepth-wallThickness,wallThickness] )
        pcbHolder(boxDepth-wallThickness*2);
}

/**
 * @param baseHeight Height of the bottom of the PCB.
 **/
module pcbHolder( totalDepth, baseHeight=1.5 ) {
    // 17x10.3 (0.8mm PCB thickness)
    wallThickness = 1;
    margin = 0.4;
    width = 10.3+margin;
    depth = 17+margin;
    thickness = 0.8+margin;
    difference() {
        translate( [0,-totalDepth/2,0] )
            cCube( [width+wallThickness*2,totalDepth,baseHeight+thickness+wallThickness] );
        translate( [0,wallThickness-depth/2-wallThickness/2,baseHeight] )
            cCube( [width,depth,thickness] );
        translate( [0,-totalDepth/2,baseHeight+wallThickness] )
            cCube( [width-wallThickness,totalDepth,thickness+wallThickness*2] );
    }
}

module mountingHole() {
    difference() {
        translate( [0, boxDepth/2-wallThickness/2, 0] )
            cCube( [mountingHoleOuterDiameter, boxDepth-wallThickness, mountingHoleOuterDiameter] );

        translate( [0, 0, mountingHoleOuterDiameter/2] )
            rotate( v=[1,0,0], a=270 )
                cylinder( d=mountingHoleDiameter, h=boxDepth-wallThickness+10 );
    }
}


//intersection() {
    bottomBox();
//    cCube( [boxWidth+2,20,boxHeight+2] );
//}


//!pcbHolder(boxDepth-wallThickness);