use <handyCubes.scad>;
include <config.scad>
use <usbc.scad>
//$fn=100;

module topBox() {

    difference() {
//    union() {
        margin = 1;
        
        // Outer box
        difference() {
            translate( [0,boxDepth/2.0,wallThickness+margin/2] )
                cCube( [boxWidth-wallThickness*2-margin,boxDepth,boxHeight-wallThickness*2-margin] );

            translate( [boxWidth/2.0+margin,boxDepth-wedgeDepth,boxHeight] )
                rotate(v=[0,0,1], a=90)
                    hCube( [wedgeDepth,boxWidth+margin*2,-wedgeHeight] );
        }

        // Inner box
        difference() {
            margin = 1;
            translate( [0,boxDepth/2-wallThickness/2,wallThickness] )
                cCube( [boxWidth-wallThickness*2,boxDepth-wallThickness,boxHeight-wallThickness] );

            translate( [boxWidth/2.0+margin,boxDepth-wedgeDepth-wallThickness,boxHeight-wallThickness] )
                rotate(v=[0,0,1], a=90)
                    hCube( [wedgeDepth,boxWidth+margin*2,-wedgeHeight] );
        }
        
        ventDepth = boxDepth-ventSlotDepthStart;
        for( i = [0:ventSlotCount-1]) {
            x = -(ventSlotCount-1)/2*ventSlotWidth*2 + i*ventSlotWidth*2;
            // Vent slots bottom
            translate( [x,boxDepth-wallThickness,ventSlotHeightStart2] )
                cCube( [ventSlotWidth,ventDepth, ventSlotHeightEnd2] );
        }
        
        // Hole for USB type-C connector
        translate( [0, boxDepth, 6] )
            usbc( 10 );
        
        
        // Mounting holes
        translate( [boxWidth/2-wallThickness-mountingHoleOuterDiameter/2, 0, wallThickness+mountingHoleOuterDiameter/2] )
            rotate( v=[1,0,0], a=270 )
                cylinder( d=mountingHoleDiameter, h=boxDepth-wallThickness+10 );
        translate( [-boxWidth/2+wallThickness+mountingHoleOuterDiameter/2, 0, wallThickness+mountingHoleOuterDiameter/2] )
            rotate( v=[1,0,0], a=270 )
                cylinder( d=mountingHoleDiameter, h=boxDepth-wallThickness+10 );
        translate( [boxWidth/2-wallThickness-mountingHoleOuterDiameter/2, 0, boxHeight-wallThickness*2] )
            rotate( v=[1,0,0], a=270 )
                cylinder( d=mountingHoleDiameter, h=boxDepth-wallThickness+10 );
        translate( [-boxWidth/2+wallThickness+mountingHoleOuterDiameter/2, 0, boxHeight-wallThickness*2] )
            rotate( v=[1,0,0], a=270 )
                cylinder( d=mountingHoleDiameter, h=boxDepth-wallThickness+10 );

       
       // Remove front plate (from "Bottom")
       translate( [0,frontWallThickness/2.0,wallThickness] )
            cCube( [boxWidth-wallThickness*2,frontWallThickness,boxHeight-wallThickness] );
    }
}


//intersection() {
    topBox();
//    translate( [0,boxDepth,0] )   // Only back plate
//        cCube( [boxWidth+2,10,boxHeight+2] );
//    translate( [0,boxDepth,0] )     // Only USB connector part (lower part of back plate)
//        cCube( [boxWidth+2,10,10] );
//}
