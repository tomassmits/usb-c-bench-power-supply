module usbc(usbCutLength = 1) {
    margin = 0.4;
    usbH = 3.2+margin;
    usbW = 9-usbH+margin;
    translate( [-usbW/2, usbCutLength/2, 0] )
    rotate( a=90, v=[1,0,0] )
    union() {
        cylinder( r=usbH/2, h=usbCutLength );
        translate( [usbW, 0, 0] )
            cylinder( r=usbH/2, h=usbCutLength );
        translate( [0, -usbH/2, 0] )
            cube( [usbW, usbH, usbCutLength] );
    }
}

usbc(10);
