/**
 * A cube that is centered on X and Y axis.
 **/
module cCube( dims, z=0 ) {
    translate( [0,0, dims[2]/2.0+z] )
        cube( dims, center=true );
}


module hCube( dims ) {
    scale( dims )
    polyhedron(
        points=[
            [0,0,0],
            [1,0,1],
            [1,0,0],
            [0,1,0],
            [1,1,1],
            [1,1,0]
        ],
        faces=[
            [0,1,2],
            [4,3,5],
            [1,4,5,2],
            [1,0,3,4],
            [0,2,5,3]
        ]
    );
}
